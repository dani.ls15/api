import { Component, OnInit } from '@angular/core';
import { RickService } from 'src/app/services/rick.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public personajes : any [] = [];

  constructor(private _Rick : RickService) { }

  ngOnInit(): void {
    this.getPersonajes();
  }
  
  getPersonajes(){
    this._Rick.getpersonajes()
      .subscribe((data:any)=>{
        console.log(data);
        this.personajes = data.results;
      });
  }

}
