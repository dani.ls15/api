import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class RickService {

  public uri = 'https://rickandmortyapi.com/api';

  constructor( private _http : HttpClient ) { }

  getpersonajes(){
   const url = `${this.uri}/character`;
   return this._http.get(url);
  }
}
